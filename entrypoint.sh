#!/bin/bash
echo "Docker cron container has been started"

declare -p | grep -Ev 'BASHOPTS|BASH_VERSINFO|EUID|PPID|SHELLOPTS|UID' > /container.env
# Setup a cron schedule
# https://www.freeformatter.com/cron-expression-generator-quartz.html
echo "SHELL=/bin/bash
PYTHONPATH=/app
BASH_ENV=/container.env
* * * * * python /app/hardware_files/trigger_lock.py >> /var/log/cron.log 2>&1
# This extra line makes it a valid cron" > scheduler.txt
crontab scheduler.txt
cron -f


