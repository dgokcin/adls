from flask import Flask, render_template, jsonify, request, redirect
from flask import make_response, abort, Response, url_for
from flask_bootstrap import Bootstrap
from flask_login import UserMixin, login_user, LoginManager, current_user
from flask_login import login_required, logout_user
from entities.forms import *
from api.Login_API import *
from api.Schedules_API import *
from api.Email_API import *
from api.Users_API import *
from api.Hardware_Interaction_API import *
from api.LockStatusHistory_API import *
from utilities.ScheduleParser import *
import face_recognition

import json

app = Flask(__name__)
SECRET_KEY = os.urandom(32)
app.config['SECRET_KEY'] = SECRET_KEY
login = LoginManager(app)


@app.before_request
def _db_connect():
    db.connect()


@app.teardown_request
def _db_disconnect(exc):
    if not db.is_closed():
        db.close()


@login.user_loader
def load_user(email):
    admin = is_admin(email)
    return UserAdapter(email, admin)


@login.unauthorized_handler
def unauthorized_callback():
    return redirect('/login?next=' + request.path)


@app.route('/')
@login_required
def index():
    return render_template(
        'index.html', lock_status=json.dumps(
            get_all_locks_as_dict()))


@app.route("/send-email", methods=['GET', 'POST'])
@login_required
def send_email():
    form = EmailForm()

    return render_template('email_page.html', form=form)


@app.route("/logout")
@login_required
def logout():
    logout_user()
    resp = make_response(redirect('/login'))
    resp.delete_cookie('is_admin')
    return resp


@app.errorhandler(401)
def custom_401(error):
    return render_template('forbidden_page.html'), 401


@app.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        if authenticate_user(form.email.data, form.password.data):
            user = load_user(form.email.data)
            login_user(user)
            resp = make_response(redirect(url_for('index')))
            resp.set_cookie('is_admin', str(current_user.admin))
            return resp
    return render_template('login.html', form=form)


@app.route('/manual-override')
@login_required
def manual_ovveride():
    if not current_user.admin:
        return abort(401)

    return render_template(
        "manual_override.html",
        locks_data=json.dumps(
            get_all_locks_as_dict()))


@app.route('/api/set-lock-status-by-name', methods=["POST"])
def set_lock_status_switch():
    lock_name = request.json["lock_name"]
    lock_status = request.json["lock_status"]
    lock_status = "Open" if lock_status == "Closed" else "Closed"
    set_lock_status_by_name(lock_name, lock_status)
    return jsonify(lock_status)


@app.route("/api/get-schedules")
def get_schedules():
    data = get_lock_schedule_history()
    return jsonify(data)


@app.route("/active-schedule", methods=['GET', 'POST'])
@login_required
def active_schedule():
    schedule_form = CreateScheduleForm()
    print(schedule_form.start.data)
    sch_sche = get_schedules_combined()
    old_sche_sche = get_schedules_combined(active=False)
    if schedule_form.submit_schedule.data and schedule_form.validate():
        lock_name = schedule_form.lock_name.data
        start = schedule_form.start.data
        end = schedule_form.end.data
        update_schedule_item(schedule_form.schedule_id.data, lock_name, start,
                             end)
        return redirect('/active-schedule')

    return render_template("active_schedule.html",
                           sch_items=json.dumps(sch_sche),
                           old_sch_items=json.dumps(old_sche_sche),
                           form=schedule_form)


@app.route("/api/get-lock-histories")
def get_lock_histories():
    data = get_lock_status_history_for_all_locks()
    data = {"data": data}
    return jsonify(data)


@app.route("/api/set-status", methods=["POST"])
def set_status():
    email = request.json["email"]
    current_status = request.json["status"]

    change_user_permission(email, current_status)

    return jsonify(success=True)


@app.route("/api/update-users", methods=["POST"])
def update_user():
    data = request.json
    update_user_info(**data)
    return jsonify(get_user_sub_info())


@app.route("/api/update-password", methods=["POST"])
def update_password():
    data = request.json
    print(data)
    update_user_password(**data)
    return jsonify(success=True)


@app.route("/api/review-data", methods=["POST"])
def review_data():
    from pathlib import Path
    f = "files/review_csv.csv"

    request.files["file_field"].save(f)
    read_data = []
    for l in open(f):
        if l[0] == "a":
            continue
        l_data = l.strip().split(",")
        tmp = {}
        tmp["lock_name"] = l_data[0]
        tmp["start_date"] = datetime.datetime.strptime(
            l_data[1].strip(),
            "%d/%m/%Y %H:%M:%S").isoformat()
        tmp["end_date"] = datetime.datetime.strptime(
            l_data[2].strip(),
            "%d/%m/%Y %H:%M:%S").isoformat()
        read_data.append(tmp)
    return jsonify(read_data)


@app.route("/view-schedules")
@login_required
def view_lock_schedules():
    return render_template('schedule_table.html')


@app.route("/users")
@login_required
def users_page():
    if not current_user.admin:
        return abort(401)
    f = open("./resources/password_data.csv", "r")
    _minval = 0
    _maxval = 0
    _special = 0
    _numbers = 0
    for l in f:
        data = l.strip().split(",")
        _minval = int(data[0])
        _maxval = int(data[1])
        _special = 1 if bool(data[2]) else 0
        _numbers = 1 if bool(data[3]) else 0
    return render_template('users_page.html',
                           user_data=json.dumps(get_user_sub_info()),
                           minval=_minval, maxval=_maxval, special=_special,
                           numbers=_numbers)


@app.route("/view-histories", methods=['GET', 'POST'])
@login_required
def view_lock_histories():
    form = EmailForm()
    if form.validate_on_submit():
        send_history_to_email(form.email.data)
    if not current_user.admin:
        return abort(401)

    return render_template('status_history_table.html', form=form)


@app.route("/set-password-restrictions", methods=['GET', 'POST'])
@login_required
def set_password_restrictions():
    f = open("./resources/password_data.csv", "r")
    _minval = 0
    _maxval = 0
    _special = True
    _number = True
    for l in f:
        data = l.strip().split(",")
        _minval = int(data[0])
        _maxval = int(data[1])
        _special = data[2]
        _number = data[3]
    form = PasswordRestrictionForm(minimum=_minval, maximum=_maxval,
                                   special_character=_special, numbers=_number)
    if form.validate_on_submit():
        f = open("./resources/password_data.csv", "w")
        minimum = str(form.minimum.data)
        maximum = str(form.maximum.data)
        special = "True" if bool(form.special_character.data) else ""
        number = "True" if bool(form.numbers.data) else ""
        to_write = [minimum, maximum, special, number]
        f.write(",".join(to_write))
        return redirect('/')
    if not current_user.admin:
        return abort(401)

    return render_template('password_restrictions.html', form=form)


@app.route('/create-schedule', methods=['GET', 'POST'])
@login_required
def create_schedule():
    csv_form = CSVUploadForm()
    schedule_form = CreateScheduleForm()
    if csv_form.submit_csv.data and csv_form.validate():
        return redirect('/view-schedules')
    if schedule_form.submit_schedule.data and schedule_form.validate():
        lock_name = schedule_form.lock_name.data
        start = schedule_form.start.data
        end = schedule_form.end.data
        add_schedule_item_tuple(lock_name, start, end)
        return redirect('/view-schedules')
    if not current_user.admin:
        return abort(401)

    return render_template('create_schedule.html', csv_form=csv_form,
                           schedule_form=schedule_form,
                           sch_items=json.dumps(get_schedules_combined()),
                           )


@app.route('/user-form', methods=['GET', 'POST'])
@login_required
def create_user():
    form = UserForm()

    if form.validate_on_submit():
        insert_user_information(form.email.data, form.password.data,
                                form.name.data, form.surname.data,
                                form.address.data, form.photo.data)
        return redirect('/users')

    if not current_user.admin:
        return abort(401)

    return render_template('create_users.html', form=form)


class UserAdapter(UserMixin):
    def __init__(self, email, admin):
        self.email = email
        self.admin = admin

    @property
    def id(self):
        return self.email


if __name__ == "__main__":
    Bootstrap(app)
    app.run(host='0.0.0.0', debug=True)
