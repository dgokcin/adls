import sys
import os
import cv2
import keras
from FaceExtractor import extract_faces
from numpy import expand_dims
from keras_vggface.vggface import VGGFace
from keras_vggface.utils import decode_predictions


imagePath = sys.argv[1]
cascPath = "/home/sam/Documents/bitbucket_adls/adls/" \
    "resources/haar_frontal_face_cascade.xml"


found_faces = extract_faces(imagePath, cascPath)
dim = (224, 224)


vgg_format_face_image = cv2.resize(found_faces[0],
                                   dim, interpolation=cv2.INTER_AREA)
samples = expand_dims(vgg_format_face_image, axis=0)


model = VGGFace(model='resnet50')
yhat = model.predict(samples)


results = decode_predictions(yhat)
prob = results[0][0][1]
is_admin = True if prob > 0.8 else False
print(results)
print(is_admin)
