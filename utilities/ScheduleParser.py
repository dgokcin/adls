import csv
import datetime


class ScheduleActionObject:
    def __init__(self, room_no, time, status):
        self.lock_name = room_no
        self.schedule_timestamp = time
        self.lock_new_status = status

    def __call__(self, *args, **kwargs):
        return {"lock_name": self.lock_name,
                "schedule_timestamp": self.schedule_timestamp,
                "lock_status_name": self.lock_new_status}


def is_csv(file_path):
    csvfile = open(file_path, 'rb')
    try:
        dialect = csv.Sniffer().sniff(csvfile.read(1024), delimiters=',')
        csvfile.seek(0)
    except csv.Error:
        print("File is not Excel csv format")
        # File appears not to be in CSV format; move along


def get_action_object_from_csv():
    rooms = []
    starts = []
    ends = []
    with open("files/temp_csv.csv") as csvfile:
        next(csvfile)  # skip the first row
        readcvs = csv.reader(csvfile)
        for row in readcvs:
            rooms.append(str(row[0]))
            starts.append(str(row[1]))
            ends.append(str(row[2]))

    schedule_action_objects = []

    n_schedules = len(rooms)

    for i in range(n_schedules):
        # Calling the created objects in order to return a dict
        schedule_action_objects.append(
            ScheduleActionObject(rooms[i],
                                 datetime.datetime.strptime(
                                     starts[i].strip(),
                                     "%d/%m/%Y %H:%M:%S"),
                                 "Open")())
        schedule_action_objects.append(
            ScheduleActionObject(rooms[i],
                                 datetime.datetime.strptime(
                                     ends[i].strip(),
                                     "%d/%m/%Y %H:%M:%S"),
                                 "Closed")())
    return schedule_action_objects
