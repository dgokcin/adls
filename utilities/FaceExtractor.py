import cv2
import os


def extract_faces(imagePath, cascPath):
    faceCascade = cv2.CascadeClassifier(cascPath)
    image = cv2.imread(imagePath)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor=1.1,
        minNeighbors=5,
        minSize=(30, 30),
        flags=cv2.CASCADE_SCALE_IMAGE
    )

    crop_face_images = []
    for (x, y, w, h) in faces:
        cropped = image[y:y+h, x:x+w]
        crop_face_images.append(cropped)
    return crop_face_images
