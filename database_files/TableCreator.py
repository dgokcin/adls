from peewee import *

from .db import db
# db = MySQLDatabase("ADLS", user="root", password="root",
#                    host="db", port=3306)


class UserTypes(Model):
    user_type_name = CharField()

    class Meta:
        database = db


class Users(Model):
    email = CharField(unique=True)
    password = CharField(500)
    name = CharField()
    surname = CharField()
    company_id = CharField()
    address = TextField()
    user_type_id = ForeignKeyField(UserTypes, backref="user_types")

    class Meta:
        database = db


class LockStatuses(Model):
    status_name = CharField()

    class Meta:
        database = db


class Locks(Model):
    lock_name = CharField()
    lock_location = CharField()
    current_status_id = ForeignKeyField(LockStatuses, backref="lock_statuses")

    class Meta:
        database = db


class LockStatusHistory(Model):
    lock_id = ForeignKeyField(Locks, backref="statuses")
    action_timestamp = DateTimeField()
    prev_status_id = ForeignKeyField(LockStatuses, backref="prev_status")
    new_status_id = ForeignKeyField(LockStatuses, backref="new_status")
    action_performed_by = ForeignKeyField(Users, backref="status_actions")

    class Meta:
        database = db


class ScheduleStatuses(Model):
    schedule_status_name = CharField()

    class Meta:
        database = db


class Schedules(Model):
    lock_id = ForeignKeyField(Locks, backref="schedules")
    schedule_timestamp = DateTimeField()
    lock_status_id = ForeignKeyField(LockStatuses,
                                     backref="status_to_set")
    schedule_status_id = ForeignKeyField(ScheduleStatuses,
                                         backref="schedule_statuses")

    class Meta:
        database = db
