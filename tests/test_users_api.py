from api.Users_API import *


def test_get_all_fields_of_existent_user_by_email():
    email = "dummy_regular@dummy.com"

    user_fields = get_all_user_fields_by_email(email)

    expected = {"email": "dummy_regular@dummy.com",
                "name": "Regular",
                "surname": "User",
                "address": "ozu"}

    assert expected == user_fields


def test_get_all_fields_of_nonexistent_user_by_email():
    email = "invalid_user@a.com"
    user_fields = get_all_user_fields_by_email(email)

    assert False is user_fields


def test_existent_user():
    email = "dummy_admin@dummy.com"
    exists = check_existent_user(email)

    assert True is exists


def test_non_existent_user():
    email = "invalid_user@a.com"
    exists = check_existent_user(email)

    assert False is exists

# TODO: RE-WORK TESTS!


# def test_update_existent_user():
#    with db.atomic() as transaction:
#        user_id = update_user_information(email="dummy_regular@dummy.com",
#                                          password="12345678",
#                                          name="Regular",
#                                          surname="User",
#                                          company_id=1,
#                                          address="ozu",
#                                          user_type_id=2)
#        transaction.rollback()
#    assert user_id is not None
#
#
# def test_update_nonexistent_user():
#    with db.atomic() as transaction:
#        update_user_information(email="nonexistent@dummy.com",
#                                password="12345678",
#                                name="Regular",
#                                surname="User",
#                                company_id=1,
#                                address="ozu",
#                                user_type_id=2)
#        success = check_existent_user("nonexistent@dummy.com")
#        transaction.rollback()
#    assert success is True
