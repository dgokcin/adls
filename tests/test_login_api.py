from api.Login_API import *


def test_verify_true_hashed_password():
    hashed = hash_password("Sup3rS3cr3TPassw0rd")
    assert True is verify_password(hashed, "Sup3rS3cr3TPassw0rd")


def test_verify_wrong_hashed_password():
    hashed = hash_password("Sup3rS3cr3TPassw0rd")
    assert False is verify_password(hashed, "WrongPassword")


def test_successful_user_authentication():
    email = "dummy_admin@dummy.com"
    password = "Sup3rS3cr3TPassw0rd"
    user_type_id = authenticate_user(email, password)

    assert 1 == user_type_id


def test_failed_user_authentication():
    email = "a@a.com"
    password = "invalidPassword"

    user_type_id = authenticate_user(email, password)
    assert False is user_type_id
