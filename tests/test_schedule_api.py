import datetime
from api.Schedules_API import *


def test_add_valid_schedule():
    with db.atomic() as transaction:
        dt = datetime.now()
        a = [{"lock_name": "225", "schedule_timestamp": dt,
              "lock_status_name": "Open"},
             {"lock_name": "225", "schedule_timestamp": dt,
              "lock_status_name": "Closed"}]
        success = add_new_schedule_items(a)
        transaction.rollback()
        assert success is True


def test_get_lock_schedule_for_time_failure():
    time = datetime(1925, 3, 28, 18, 0, 0)
    sch = get_lock_schedule_at_time(time)
    assert len(sch) == 0


def test_get_schedule_history():
    sch = get_lock_schedule_history()
    assert len(sch) != 0
