from api.Locks_API import *


def test_get_all_lock_names_as_string():
    lock_names = get_all_lock_names_as_string()
    assert len(lock_names) != 0


def test_get_all_locks_as_dict():
    locks = get_all_locks_as_dict()
    assert len(locks) != 0


def test_valid_get_lock_status_by_lock_name():
    lock_name = "225"
    status_name = get_lock_status_by_lock_name(lock_name)
    assert status_name != ""


def test_invalid_get_lock_status_by_lock_name():
    lock_name = "999"
    status_name = get_lock_status_by_lock_name(lock_name)
    assert status_name == ""


def test_valid_get_locks_by_status_name():
    locks = get_locks_by_status_name("Open")
    assert locks is not None


def test_invalid_get_locks_by_status_name():
    locks = get_locks_by_status_name("NotOpen")
    assert locks is None


def test_valid_set_lock_status_by_name():
    with db.atomic() as transaction:
        lock_name = "225"
        new_status_name = "Open"
        success = set_lock_status_by_name(lock_name, new_status_name)
        transaction.rollback()
        assert success is True


def test_invalid_set_lock_status_by_name_1():
    with db.atomic() as transaction:
        lock_name = "999"
        new_status_name = "Open"
        success = set_lock_status_by_name(lock_name, new_status_name)
        transaction.rollback()
        assert success is False


def test_invalid_set_lock_status_by_name_2():
    with db.atomic() as transaction:
        lock_name = "225"
        new_status_name = "NotOp"
        success = set_lock_status_by_name(lock_name, new_status_name)
        transaction.rollback()
        assert success is False


def test_invalid_set_lock_status_by_name_3():
    with db.atomic() as transaction:
        lock_name = "999"
        new_status_name = "NotOp"
        success = set_lock_status_by_name(lock_name, new_status_name)
        transaction.rollback()
        assert success is False


def test_valid_set_status_for_all_locks():
    with db.atomic() as transaction:
        new_status_name = "Open"
        success = set_status_for_all_locks(new_status_name)
        transaction.rollback()
        assert success is True


def test_invalid_set_status_for_all_locks():
    with db.atomic() as transaction:
        new_status_name = "NotOp"
        success = set_status_for_all_locks(new_status_name)
        transaction.rollback()
        assert success is False
