from api.LockStatusHistory_API import *


def test_valid_get_lock_status_history_by_lock_name():
    history = get_lock_status_history_by_lock_name("225")
    assert len(history) > 0


def test_invalid_get_lock_status_history_by_lock_name():
    history = get_lock_status_history_by_lock_name("999")
    assert history is None


def test_get_lock_status_history_for_all_locks():
    history = get_lock_status_history_for_all_locks()
    assert len(history) > 0


def test_valid_add_lock_status_history():
    with db.atomic() as transaction:
        lock_name = "225"
        old_status_name = "Closed"
        new_status_name = "Open"
        user = "Admin"
        success = add_lock_status_history(lock_name, old_status_name,
                                          new_status_name, user)
        transaction.rollback()
        assert success is True


def test_invalid_add_lock_status_history_1():
    with db.atomic() as transaction:
        lock_name = "999"
        old_status_name = "Closed"
        new_status_name = "Open"
        user = "Admin"
        success = add_lock_status_history(lock_name, old_status_name,
                                          new_status_name, user)
        transaction.rollback()
        assert success is False


def test_invalid_add_lock_status_history_2():
    with db.atomic() as transaction:
        lock_name = "225"
        old_status_name = "Close"
        new_status_name = "Open"
        user = "Admin"
        success = add_lock_status_history(lock_name, old_status_name,
                                          new_status_name, user)
        transaction.rollback()
        assert success is False


def test_invalid_add_lock_status_history_3():
    with db.atomic() as transaction:
        lock_name = "225"
        old_status_name = "Closed"
        new_status_name = "Ope"
        user = "Admin"
        success = add_lock_status_history(lock_name, old_status_name,
                                          new_status_name, user)
        transaction.rollback()
        assert success is False


def test_invalid_add_lock_status_history_4():
    with db.atomic() as transaction:
        lock_name = "225"
        old_status_name = "Closed"
        new_status_name = "Open"
        user = "Admi"
        success = add_lock_status_history(lock_name, old_status_name,
                                          new_status_name, user)
        transaction.rollback()
        assert success is False
