#!/bin/bash

# Get the tag from the branchname
TAG=$(echo "$BITBUCKET_BRANCH" | awk -F'/' '{print $2}')

# Login with the variables stored in bitbucket
docker login -u ${DOCKER_USERNAME} -p ${DOCKER_PASSWORD}

# Build the images with latest tag
docker build -t ${DOCKER_USERNAME}/adls-app:latest .
docker build -t ${DOCKER_USERNAME}/adls-cron:latest -f Dockerfile.cron .

# Push the images with the latest tag
docker push ${DOCKER_USERNAME}/adls-app:latest
docker push ${DOCKER_USERNAME}/adls-cron:latest

# Retag the images with the TAG variable
docker tag ${DOCKER_USERNAME}/adls-app:latest ${DOCKER_USERNAME}/adls-app:${TAG}
docker tag ${DOCKER_USERNAME}/adls-cron:latest ${DOCKER_USERNAME}/adls-cron:${TAG}

# Push the images with the new tag
docker push ${DOCKER_USERNAME}/adls-app:${TAG}
docker push ${DOCKER_USERNAME}/adls-cron:${TAG}

