from xmlrpc.server import SimpleXMLRPCServer
from hardware_files.Pi.device import Device


def set_servo_angle(angle):
    global dev
    dev.set_servo_angle(angle)


def set_servo_open(status):
    global dev
    dev.set_servo_open(status)


def sense_us_range():
    global dev
    return dev.sense_us_range()


def get_door_status():
    global dev
    return dev.get_door_status()


def take_picture():
    global dev
    dev.take_picture()


def one_time_unlock():
    global dev
    dev.one_time_unlock()


if __name__ == "__main__":
    dev = Device(2, 18, 24, 20, 21, "", 0)
    server = SimpleXMLRPCServer(("192.168.2.101", 8000), allow_none=True)
    server.register_function(set_servo_angle, "set_servo_angle")
    server.register_function(set_servo_open, "set_servo_open")
    server.register_function(sense_us_range, "sense_us_range")
    server.register_function(get_door_status, "get_door_status")
    server.register_function(take_picture, "take_picture")
    server.register_function(one_time_unlock, "one_time_unlock")
    try:
        server.serve_forever()
    except KeyboardInterrupt as k:
        device.exit()
