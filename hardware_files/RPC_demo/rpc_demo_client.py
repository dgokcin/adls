import xmlrpc.client


def send_command_to_hardware(user, status):
    with xmlrpc.client.ServerProxy("http://192.168.2.101:8000/") as proxy:
        if user == 1:
            angle = int(input("Angle: "))
            proxy.set_servo_angle(angle)
        if user == 2:
            # status = int(input("Open 1, Close 0: "))
            proxy.set_servo_open(status)
        if user == 3:
            print("Range is ", proxy.sense_us_range())
        if user == 4:
            print("Door status ", proxy.get_door_status())
        if user == 5:
            proxy.take_picture()
        if user == 6:
            proxy.one_time_unlock()
