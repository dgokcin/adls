import datetime
from api.Schedules_API import *
from api.Locks_API import *

schedule = get_current_lock_schedule()

for key, value in schedule.items():
    if value[0] == 1:
        set_lock_status_by_name(str(key), 'Open')
    elif value[0] == 2:
        set_lock_status_by_name(str(key), 'Closed')
        trigger_schedule_status(value[1] - 1)
