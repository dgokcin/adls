import pigpio
import os
import time
import threading
import pickle as pkl
import glob
import face_recognition
import cv2


class Device:
    """This is a class that contains all of the hardware realted
       functions and values

        Parameters
        ----------
        servo_pin : int
            Pin number corresponding to the servo motor
        us_trigger_pin : int
            Pin number of the trigger pin of the ultrasonic sensor
        us_echo_pin : int
            Pin number of the echo pin of the ultrasonic sensor
        btn_in_pin : int
            Pin number of the button inside the room
        btn_out_pin : int
            Pin number of the button outside the room
        ip : str
            The IP of the server as a string in 'xxx.xxx.xxx.xxx' format
        port : int
            The port of the server

        Attributes
        ----------
        io : pigpio.pi
            The Pie GPIO object for sending signals to and reciving singals
            from sensors
        io_lock : threading.Lock
            This is a lock for interracting with the I/O
        locking_thread_running : bool
            A flag for the status of the thread used by the one time lock
        server : HardwareServer
            A server thread for communicating with the backend

    """

    def __init__(self, servo_pin, us_trigger_pin, us_echo_pin,
                 btn_in_pin, btn_out_pin, ip, port):
        self.io = pigpio.pi()
        self.io_lock = threading.Lock()
        self.servo_pin = servo_pin
        self.us_trigger_pin = us_trigger_pin
        self.us_echo_pin = us_echo_pin
        self.io.set_mode(us_trigger_pin, pigpio.OUTPUT)
        self.io.set_mode(us_echo_pin, pigpio.INPUT)
        self.door_name = "225"
        # set button callback functions
        self.io.callback(btn_out_pin, pigpio.RISING_EDGE, self.request_button)
        self.io.callback(btn_in_pin, pigpio.RISING_EDGE, self.one_time_unlock)
        # add debounce filter to buttons
        self.io.set_glitch_filter(btn_out_pin, 100)
        self.io.set_glitch_filter(btn_in, 100)
        self.locking_thread_running = False
        #        self.server = HardwareServer(ip, port)
        print("Device initialized")

        def exit(self):
            print("Exitting...")
            io.cleanup()

    def set_servo_angle(self, angle, duty_min=500, duty_mult=1600/180):
        """Will set the servo angle to the specified angle

        Parameters
        ----------
        angle : int
            The desired motor angle
        duty_min : int
            The minimum duty cycle. By defautl it is 500
        duty_mult : double
            The duty cycle multiplier for calculating the duty cycle from raw
            angle
        """

        # Acquire the I/O lock
        self.io_lock.acquire()

        # Limit the angle to 0-180 range
        angle = min(180, angle)
        angle = max(0, angle)

        duty = duty_min + duty_mult * angle
        self.io.set_servo_pulsewidth(self.servo_pin, duty)

        # Release the I/O lock
        self.io_lock.release()

    def set_servo_open(self, arg):
        """Sets the servo angle to either open or close the door

        Parameters
        ----------
        arg : bool
            True for open, false for close
        """
        # enum = LockSignals.UNLOCK if arg else LockSignals.LOCK
        if arg:
            self.set_servo_angle(0)
        else:
            self.set_servo_angle(180)
        # receive_lock_update_signal(self.door_name, enum)

    def sense_us_range(self, timeout=5):
        """Function used for detecting the range the Ultrasonic
           sensor senses

        Returns
        -------
        int
            The distance in terms of cm
        """
        # Acquire the I/O lock
        self.io_lock.acquire()

        # Set the trigger pin
        self.io.write(self.us_trigger_pin, 1)

        # Unset the trigger pin after 10us
        time.sleep(0.00001)
        self.io.write(self.us_trigger_pin, 0)

        pulse_init = time.time()
        pulse_start = 0

        # Detect echo positve edge
        while self.io.read(self.us_echo_pin) == 0:
            pulse_start = time.time()
            if pulse_start - pulse_init > timeout:
                self.io_lock.release()
                return -1

        pulse_end = 0
        pulse_init = pulse_start

        # Detect echo negative edge
        while self.io.read(self.us_echo_pin) == 1:
            pulse_end = time.time()
            if pulse_end - pulse_init > timeout:
                self.io_lock.release()
                return -1

        # Calculate distance using speed of sonic
        distance = (pulse_end - pulse_start) * 34300 / 2

        # Release the I/O lock
        self.io_lock.release()

        return distance

    def get_door_status(self, threshold=5):
        """Function which compares the Ultrasonic Sensor reading to the
           threshold for detecting the door status

        Parameters
        ----------
        threshold : int
            The threshold for detecting if the door is open or not

        Returns
        -------
        bool
            True for open, false for closed

        """

        return self.sense_us_range() > threshold

    def take_picture(self, res="1280x720", path="~/webcam/snap.jpg"):
        """Takes a camera picture in specified resolution and saves it to the
           specified path

        Parameters
        ----------
        res : str
            The target resolution in WxH format
        path :
            The path of the output picture
        """

        cmd = "fswebcam -r {} {}".format(res, path)
        os.system(cmd)

    def wait_for_closed(self):
        """This function checks the status of the door until
           it detects that it is closed. Afterwards it locks it.
        """
        while True:
            status = self.get_door_status()
            if not status:
                break
            time.sleep(2)
        self.set_servo_open(False)
        self.locking_thread_running = False

    def one_time_unlock(self, gpio=None, level=None, tick=None):
        """This function unlocks the door for a shor period of time and then
           locks the door once the door is closed.

        Parameters
        ----------
        gpio :
            Optional argument needed to satisfy callback structure.
            Otherwise not used.
        level :
            Optional argument needed to satisfy callback structure.
            Otherwise not used.

        tick :
            Optional argument needed to satisfy callback structure.
            Otherwise not used.

        """
        if self.locking_thread_running is False:
            self.locking_thread_running = True
        else:
            return
        if self.get_door_status():
            print("Warning: One Time Unlock: Door already open")
            return
        self.set_servo_open(True)
        time.sleep(5)
        locking_thread = threading.Thread(target=self.wait_for_closed)
        locking_thread.start()

    def get_faces(self):
        return [pkl.load(open(face, "rb")) for face in glob.glob(
            "/home/pi/faces/*.pkl")]

    def request_button(self, gpio=None, level=None, tick=None):
        print("button out")
        if self.get_door_status():
            print("Warning: Request Button: Door already open")
            return
        self.take_picture()
        photo = cv2.imread("/home/pi/webcam/snap.jpg")
        try:
            photo_enc = face_recognition.face_encodings(photo)[0]
            face_recognized = True in face_recognition.compare_faces(
                photo_enc, self.get_faces())
            if face_recognized:
                self.one_time_unlock()
            else:
                print("face not recoginized")
        except IndexError as error:
            print("No face found")
