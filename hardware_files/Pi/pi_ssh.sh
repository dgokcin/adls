#!/bin/sh

# Script for initiating SSH connection automatically by looking up Raspberry Pi's IP
# User will be prompted for a password when connecting

# The MAC address of the Wi-Fi adapter
PI_MAC=b8:27:eb:6c:e0:9e

# Get the ip based on the  MAC address
PI_IP=$(arp -a | grep $PI_MAC | cut -d' ' -f2 | sed -r "s/(^.)|(.$)//g")

# Start SSH with X11 forwarding
ssh -Y pi@$PI_IP

