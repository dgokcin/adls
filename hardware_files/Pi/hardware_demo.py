from device import Device

if __name__ == "__main__":
    dev = Device(2, 18, 24, 20, 21)

    while True:
        user = input(
            "1. Set angle\n2. Open - Close\n3. Sense range\n4. Door status\n\
                    5. Take picture\n6. One time unlock\n")
        user = int(user)
        if user == 1:
            angle = int(input("Angle: "))
            dev.set_servo_angle(angle)
        if user == 2:
            status = int(input("Open 1, Close 0: "))
            dev.set_servo_open(status)
        if user == 3:
            print("Range is ", dev.sense_us_range())
        if user == 4:
            print("Door status ", dev.get_door_status())
        if user == 5:
            dev.take_picture()
        if user == 6:
            dev.one_time_unlock()
