import socket
import sys
import base64
from entities.lock_signals import Enum, LockSignals
import threading
import queue


class HardwareServer(threading.Thread):
    """
    This class is a thread that acts as a server that will run
    on the Raspberry Pie. The server will be able to receive signals
    from the webserver and act or respond accordingly.

    Parameters
    ----------
    ip : str
        The IP of the server as a string in 'xxx.xxx.xxx.xxx' format
    port : int
        The port of the server
    io_lock : threading.lock
        Mutex lock for accessing the hardware IO

    Attributes
    ----------
    s: socket.socket
        Socket object of the server
    message_queue: queue containing outgoing messages

    """

    def __init__(self, ip, port):
        threading.Thread.__init__(self)
        self.s = socket.socket()
        self.ip = ip
        self.port = port
        self.s.bind((self.ip, self.port))
        self.s.listen(1)
        self.message_queue = queue()

    def accept_connection(self):
        """Function for receving packets
        """
        client, addr = self.s.accept()
        request_byte = client.recv(8)
        request = request_byte.decode(sys.stdout.encoding)
        self.message_handler(request, client)
        client.close()

    def run(self):
        """Function called when the thread is started
        """
        while True:
            if not self.message_queue.empty():
                msg = message_queue.get()
                # self.send(msg)
                self.message_queue.task_done()
            else:
                self.accept_connection()

    def message_handler(self, request, client):
        """Function for determining what to do with incoming message

        Parameters
        ----------
        request : str
            String message to be processed
        client : socket.socket
            Socket for sending sending response to

        ReturnS
        -------
        bool
            Boolean value indicating the success of message handling
        """
        # The incoming request should be a number type
        try:
            request = int(request)
        except ValueError:
            print("Invalid signal received")
            return False

        if (request == LockSignals.LOCK):
            # call lock door function
            print("Lock signal received")
            return True
        if (request == LockSignals.UNLOCK):
            # call unlock door function
            print("Unlock signal received")
            return True
        if (request == LockSignals.ONE_TIME_UNLOCK):
            # call one time lock function
            print("One time unlock signal received")
            return True
        else:
            print("Unkown signal received")
            return False
