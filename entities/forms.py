from flask_wtf import FlaskForm
from wtforms import *
from wtforms.validators import DataRequired, EqualTo
from wtforms.fields.html5 import EmailField, DateTimeLocalField
from wtforms.fields import SelectField, BooleanField
from flask_wtf.file import FileField
from utilities import ScheduleParser
from api import Users_API
from api import Locks_API
from api import Schedules_API
import re

door_status = [("Open", "Open"), ("Closed", "Closed")]  # integrate this


class CSVUploadForm(FlaskForm):
    file_field = FileField("Load CSV", id="csv_field")
    submit_csv = SubmitField("Submit")

    def validate_file_field(self, field):
        self.file_field.data.save("files/temp_csv.csv")
        schedule_objects = ScheduleParser.get_action_object_from_csv()
        added = Schedules_API.add_new_schedule_items(schedule_objects)
        if not added:
            raise ValidationError(
                "Conflict Detected! Please Update Your CSV File!")


class EmailForm(FlaskForm):
    email = EmailField('E-Mail')
    lock_name = TextField('Lock ID')
    text = TextAreaField('Text')
    submit = SubmitField("Send Email")


class LoginForm(FlaskForm):
    email = EmailField('', [validators.DataRequired(), validators.Email()])
    password = PasswordField('', [validators.DataRequired()])
    submit = SubmitField("Login")


class UserForm(FlaskForm):
    email = EmailField(
        'E-Mail', [validators.DataRequired(), validators.Email()])
    name = TextField('Name', [validators.DataRequired()])
    surname = TextField('Surname', [validators.DataRequired()])
    password = PasswordField(
        'Password', [
            validators.DataRequired()])
    confirm = PasswordField(
        'Confirm Password', [
            validators.DataRequired(), EqualTo(
                'password', message="Passwords Must Match!")])
    address = TextAreaField('Address', [validators.DataRequired()])
    photo = FileField('Face Photo', [validators.DataRequired()])
    submit = SubmitField("Register")

    def has_numbers(self, inputString):
        return any(char.isdigit() for char in inputString)

    def validate_email(self, field):
        result = Users_API.check_existent_user(field.data)
        if result:
            raise ValidationError("E-Mail must be unique!")

    def check_splcharacter(self, input_string):

        string_check = re.compile('[@_!#$%^&*()<>?/|}{~:]')

        if string_check.search(input_string) is None:
            return False
        return True

    def validate_password(self, field):
        password = self.password.data
        f = open("./resources/password_data.csv")
        minlen = 0
        maxlen = 0
        numbers = False
        special = False
        for l in f:
            data = l.strip().split(",")
            minlen = int(data[0])
            maxlen = int(data[1])
            special = True if str(data[2]) == "True" else False
            numbers = True if str(data[3]) == "True" else False
        if not (minlen <= len(password) <= maxlen):
            raise ValidationError("Password Size Must be Between " + str(
                minlen) + "-" + str(maxlen) + " Characters")
        if numbers and not self.has_numbers(password):
            raise ValidationError("Password Must Contain Numbers")
        if special and not self.check_splcharacter(password):
            raise ValidationError("Password Must Contain Any of the Special "
                                  "Characters @_!#$%^&*()<>?/|}{~:")

    def validate_photo(self, field):
        name = self.photo.data.filename
        if name.split(".")[-1] not in ["jpeg", "png", "jpg"]:
            raise ValidationError("Image Must be .jpeg , .png , or . jpg")


class CreateScheduleForm(FlaskForm):
    schedule_id = HiddenField("Schedule ID", id="schedule-id")
    locks = Locks_API.get_all_lock_names_as_string()
    locks = [(a, a) for a in locks]

    lock_name = SelectField("Lock Name", [validators.DataRequired()],
                            choices=locks, id="lock-name")
    start = DateTimeLocalField("Event Start", [validators.InputRequired()],
                               format='%Y-%m-%dT%H:%M', id="start-date")
    end = DateTimeLocalField("Event End", [validators.InputRequired()],
                             format='%Y-%m-%dT%H:%M', id="end-date")
    submit_schedule = SubmitField("Register")

    def validate_end(self, field):
        if self.end.data < self.start.data:
            raise ValidationError("End Date Must Be Later Than Start Date!")

    def validate_start(self, field):
        if not Schedules_API.validate_schedule(self.lock_name.data,
                                               self.start.data, self.end.data):
            raise ValidationError("Conflict Detected! Please Update "
                                  "Information!")


class PasswordRestrictionForm(FlaskForm):
    minimum = IntegerField("Minimum Length")
    maximum = IntegerField("Maximum Length")
    special_character = BooleanField("Force Special Characters")
    numbers = BooleanField("Force Numbers")
    submit = SubmitField("Submit")

    def validate_minimum(self, field):
        if self.minimum.data < 0:
            raise ValidationError("Minimum Value Must be Greater Than 0!")

    def validate_maximum(self, field):
        if self.maximum.data < self.minimum.data:
            raise ValidationError("Maximum Value Must be Greater Than the "
                                  "Minimum Value")
