from enum import Enum


class LockSignals:
    LOCK = 0
    UNLOCK = 1
    ONE_TIME_UNLOCK = 2
