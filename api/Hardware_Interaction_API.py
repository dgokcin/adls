from entities.lock_ips import *
from entities.lock_signals import Enum, LockSignals
import socket
from api.Locks_API import *


def send_signal_to_lock_with_name_and_signal_type(lock_name, signal):
    """
    This method send the desired signal to the desired lock

    Parameters
    ----------
    lock_name : string
        The name of the lock to be updated.

    signal : Enum
        The enum of the signal to send. Enum Type is Lock Signals but for
        some reason docstring is not built for enums. When the name of the
        enum is given, it does not use the attributes properly so the
        general class is given. In the future, all of the api will revolve
        around these or other Enums

    Returns
    -------
    success : bool
        Returns True if the signal is send successfully, False otherwise.
        There are couple of reasons which might cause False, these
        are:
            - Wrong IP
            - Wrong Port
            - Wrong Signal
    """
    try:
        if not isinstance(signal, LockSignals):
            return False
        lock_ip = lock_ips[lock_name]
        signal_value = signal.value
        s = socket.socket()
        s.connect((lock_ip, active_port))
        message = str(signal_value).encode()
        s.sendall(message)
        s.close()
        # TODO currently static, will be fixed
        status_name = "Closed" if signal_value == 0 else "Open"
        set_lock_status_by_name(lock_name, status_name)
        return True
    except socket.timeout:
        return False


def receive_lock_update_signal(lock_name, signal):
    """
        This method received an update from lock indicating what happened to
        which lock

        Parameters
        ----------
        lock_name : string
            The name of the lock to be updated.

        signal : Enum
            The enum of the action taken. Enum Type is Lock Signals but for
            some reason docstring is not built for enums. When the name of the
            enum is given, it does not use the attributes properly so the
            general class is given. In the future, all of the api will revolve
            around these or other Enums

        Returns
        -------
        success : bool
            Returns True if the db is updated correctly, False otherwise.
            There are couple of reasons which might cause False, these
            are:
                - Wrong lock name
                - Wrong signal enum
        """
    try:
        if not isinstance(signal, LockSignals):
            return False
        signal_value = signal.value
        status_name = "Closed" if signal_value == 0 else "Open"
        set_lock_status_by_name(lock_name, status_name)
    except socket.timeout:
        return False
