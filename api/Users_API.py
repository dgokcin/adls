from database_files.TableCreator import *
from api.Login_API import hash_password
import face_recognition
import pickle as pkl
import os
import cv2


def get_all_user_fields_by_email(email):
    """
    This method returns the user data by email

    Parameters
    ----------
    email : string
        The name email of the user.

    Returns
    -------
    user_dict : dict 
        Returns a dictionary of the user info:
            user-email
            user-name
            user-surname
            user-address
    """
    try:
        user = Users.get(Users.email == email)
        return {"email": user.email,
                "name": user.name,
                "surname": user.surname,
                "address": user.address}
    except Users.DoesNotExist:
        return False


def check_existent_user(email):
    """
    This method checks the existence of a user in the database.

    Parameters
    ----------
    email : string
        The email of the user to be checked

    Returns
    -------
    success : bool
        Returns True if the user exists, false otherwise.
    """
    try:
        Users.get(Users.email == email)
    except Users.DoesNotExist:
        return False
    return True


def insert_user_information(email, password, name, surname, address, photo):
    """
    This method checks the updates the users in the database. If the user
    already exists in the database, it updates the user. If the user is a
    new user, it adds the user to the database.

    Parameters
    ----------
    email : string
        The email of the user to be added/updated
    password : string
        Password of the user. Not sure when this should be hashed.
    name : string
        The name of the user to be added/updated
    surname : string
        The surname of the user to be added/updated
    address : string
        The address of the user to be added/updated
    photo : File
        The face picture of a user
    Returns
    -------
    usert_id : bool
        Returns the id of the newly added/updated user if successful,
        None otherwise.
    """
    try:
        user_id = Users.insert(email=email,
                               password=hash_password(password),
                               name=name,
                               surname=surname,
                               company_id="1",
                               address=address,
                               user_type_id=2).execute()
        p_ext = photo.filename.split(".")[-1]
        photo.save("files/pictures/" + name + "_" + surname + "." + p_ext)

        photo_file = cv2.imread(
            "files/pictures/" + name + "_" + surname + "." + p_ext)
        photo_enc = face_recognition.face_encodings(photo_file)[0]
        pkl.dump(photo_enc, open("files/" + name + "_" + surname + ".pkl",
                                 "wb"))
        os.system("sshpass -p \"raspberry\" scp files/" + name + "_" +
                  surname + ".pkl" + " pi@192.168.2.101:/home/pi/faces")

        return user_id
    except IntegrityError:
        return None


def update_user_info(email, name, surname, address):
    """
    This method updated the name, surname and the address of an user by email.

    Parameters
    ----------
    email : string
        The email of the user to be updated

    Returns
    -------
    success : bool
        Returns True if the user exists and the update is executed without error, false otherwise.
    """
    try:
        Users.update(name=name, surname=surname, address=address).where(
            Users.email == email).execute()
        return True
    except Users.DoesNotExist:
        return False


def update_user_password(email, password):
    """
    This method changes the password of an user by email.

    Parameters
    ----------
    email : string
        The email of the user to change password

    Returns
    -------
    success : bool
        Returns True if the user exists and the update is executed without error, false otherwise.
    """
    try:
        Users.update(password=hash_password(password)).where(
            Users.email == email).execute()
        return True
    except Users.DoesNotExist:
        return False


def change_user_permission(email, curr_status):
    """
    This method changes the password of an user by email.

    Parameters
    ----------
    email : string
        The email of the user to change permission

    Returns
    -------
    success : bool
        Returns True if the user exists and the update is executed without error, false otherwise.
    """
    status = {1: 2, 2: 1}
    try:
        Users.update(user_type_id=status[curr_status]).where(
            Users.email == email).execute()
        return True
    except Users.DoesNotExist:
        return False


def get_user_sub_info():
    """
    This method returns all of the users as dictionaries.

    Parameters
    ----------

    Returns
    -------
    users_dict : list
        Returns a list of all user dictionaries
    """
    try:
        users = Users.select(Users.name, Users.surname, Users.email,
                             Users.user_type_id, Users.address).dicts()
        users_dict = [user for user in users]
        return users_dict
    except Users.DoesNotExist:
        return None


def is_admin(email):
    """
    This method returns whether the user is an admin or not.

    Parameters
    ----------
    email : string
        The email of the user to check admin status

    Returns
    -------
    success : bool
        Returns True if the user is an admin False otherwise
    """
    user = Users.get(Users.email == email)
    return True if user.user_type_id.id == 1 else False
