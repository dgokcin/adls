from string import Template
import smtplib
import ssl
from api.LockStatusHistory_API import *

port = 587
password = "Emomelo123"
sender = "adls.noreply@gmail.com"


def _read_template():
    with open("./resources/sample_admin_email.txt", 'r',
              encoding='utf-8') as template_file:
        template_file_content = template_file.read()
    return Template(template_file_content)


def _manipulate_history_data(history_data):
    data = [["Lock ID", "Action Time", "Old Status", "New Status", "User"]]
    for a in history_data:
        data.append([str(a["lock_id"]), str(a["action_timestamp"].strftime(
            "%d/%m/%y %H:%M")), str(a["prev_status_id"]),
            str(a["new_status_id"]), str(a["action_performed_by"])])
    message = ""
    for row in data:
        message += "{: ^20} {: ^20} {: ^20} {: ^20} {: ^20}".format(*row)
        message += "\n"
    return message


def send_history_to_email(email):
    """
    This method sends the history of the locks to the
    given email.

    Parameters
    ----------
    email: string

    """
    context = ssl.create_default_context()
    context.check_hostname = False
    context.verify_mode = ssl.CERT_NONE
    with smtplib.SMTP("smtp.gmail.com", port) as server:
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(sender, password)

        status_history = get_lock_status_history_for_all_locks()
        status_history = _manipulate_history_data(status_history)
        template = _read_template()
        message = template.substitute(PERSON_NAME=status_history)
        server.sendmail(sender, email, message)
