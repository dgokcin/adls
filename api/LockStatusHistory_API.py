from database_files.TableCreator import *
import datetime
import peewee


def get_lock_status_history_by_lock_name(lock_name):
    """
    This method returns all information previous information
    regarding the given lock name

    Parameters
    ----------
    lock_name : string
        The name of the the lock to query

    Returns
    -------
    data_dict : list of dict
        A Python list which contains dictionaries about each entry
        in the database about status history of the given lock.
        If name is not found, returns None
    """
    try:
        lock = Locks.get(Locks.lock_name == lock_name)
        status_history = LockStatusHistory.select().where(
            LockStatusHistory.lock_id == lock).dicts()
        data_dict = []
        for a in status_history:
            data_dict.append(a)
        return data_dict
    except (Locks.DoesNotExist, LockStatusHistory.DoesNotExist):
        return None


def get_lock_status_history_for_all_locks():
    """
    This method returns all information previous information
    regarding the given lock name

    Parameters
    ----------

    Returns
    -------
    data_dict : list of dict
        A Python list which contains dictionaries about each entry
        in the database about status history of each lock.
    """
    try:
        status_history = LockStatusHistory.select().dicts()
        data_dict = []
        for a in status_history:
            o_s = LockStatuses.get(LockStatuses.id == a[
                "prev_status_id"]).status_name
            n_s = LockStatuses.get(LockStatuses.id == a[
                "new_status_id"]).status_name
            l_m = Locks.get(Locks.id == a["lock_id"]).lock_name
            a["prev_status_id"] = o_s
            a["new_status_id"] = n_s
            a["lock_id"] = l_m
            data_dict.append(a)
        return data_dict
    except (Locks.DoesNotExist, LockStatusHistory.DoesNotExist):
        return None


def add_lock_status_history(lock_name, old_status_name, new_status_name, user):
    """
    This method returns adds a new lock status history by the
    lock name and the status changes and the user that did the
    change.


    Parameters
    ----------
    lock_name: string
    old_status_name: string
    new_status_name: string
    user: string

    Returns
    -------
    success: bool

    Returns True if the Lockstatus has been successfully
    created, false otherwise.
    """
    try:
        lock = Locks.get(Locks.lock_name == lock_name)
        old_status = LockStatuses.get(
            LockStatuses.status_name == old_status_name)
        new_status = LockStatuses.get(
            LockStatuses.status_name == new_status_name)
        user = Users.get(Users.name == user)
        current_date = datetime.datetime.now()
        LockStatusHistory.create(lock_id=lock, action_timestamp=current_date,
                                 prev_status_id=old_status.id,
                                 new_status_id=new_status.id,
                                 action_performed_by_id=user)
        return True
    except peewee.DoesNotExist:
        return False
