from database_files.TableCreator import *
import hashlib
import binascii
import os


def get_all_users_as_string():
    """
    This method returns the  ids all the users in the database.

    Parameters
    ----------

    Returns
    -------
    user_ids: list of string
        A Python list of strings object which holds all the user
        ids in the database.
    """
    users = Users.select(Users)
    user_ids = []
    for user in users:
        user_ids.append(user.id)
    return user_ids


def hash_password(password):
    """Hash a password for storing. The salt is a randomly generated string
    that is joined with the password before hashing. Being randomly generated,
    it ensures that even hashes of equal passwords get different results.
    Parameters
    ----------
    password : string
        The password to be hashed.
    Returns
    -------
    hashed_password : string
        Returns the hashed password which can be safely stored in the database
    """
    salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
    password_hash = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'),
                                        salt, 100000)
    password_hash = binascii.hexlify(password_hash)
    return (salt + password_hash).decode('ascii')


def verify_password(stored_password, provided_password):
    """Verify a stored password against one provided by user
    Parameters
    ----------
    stored_password : string
        The password taken from the database.

    provided_password : string
        The password provided by the user.

    Returns
    -------
    success : bool
        Returns True if passwords match, False otherwise.
    """
    salt = stored_password[:64]
    stored_password = stored_password[64:]
    password_hash = hashlib.pbkdf2_hmac('sha512',
                                        provided_password.encode('utf-8'),
                                        salt.encode('ascii'), 100000)
    password_hash = binascii.hexlify(password_hash).decode('ascii')
    return password_hash == stored_password


def authenticate_user(email, password):
    """
    This method checks if the password of the user matches with the password
    in the database.

    Parameters
    ----------
    email : string
        The email of the user to be checked

    password : string
        The password of the user.
    Returns
    -------
    success : bool
        Returns user_id if authentication is successful, -1 otherwise.
    """
    try:
        user_object = Users.get(Users.email == email)
        if verify_password(user_object.password, password):
            return user_object.user_type_id_id
        else:
            return False
    except Users.DoesNotExist:
        return False
