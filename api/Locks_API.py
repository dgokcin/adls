from api.LockStatusHistory_API import *
from hardware_files.RPC_demo import rpc_demo_client
from resources import variables


# TODO Also make sure the lock status updated are logged in
# lockstatushistorytable. Currently not needed but will be
# in the future
def get_all_lock_names_as_string():
    """
    This method returns the names of all the locks in the database

    Parameters
    ----------

    Returns
    -------
    lock_names: list of string
        A Python list of strings object which holds all the lock
         names in the database.
    """
    locks = Locks.select(Locks.lock_name)
    lock_names = []
    for lock in locks:
        lock_names.append(lock.lock_name)
    return lock_names


def get_all_locks_as_dict():
    """
    This method returns all details of each lock in the database
     as a list of dicts.

    Parameters
    ----------
    Returns
    -------
    locks_dict : list of dict
        A Python List of Dictionary objects which consists of
        keys which represent the columns of the Locks table
        and values represent the corresponding value for each entry

    """
    locks = Locks.select().dicts()
    locks_list = []
    for l in locks:
        l["current_status_id"] = LockStatuses.get(
            LockStatuses.id == l["current_status_id"]).status_name
        locks_list.append(l)
    return locks_list


def get_lock_status_by_lock_name(lock_name):
    """
    This method returns the lock status of a specific lock by its' name

    Parameters
    ----------
    lock_name : string
        The name of the room to be queried

    Returns
    -------
    status_name : string
        The name of the status of the current lock being
        queried. Currently it is either Open or Closed.
        If the lock name is invalid, returns empty string
    """
    try:
        statuses = Locks.get(Locks.lock_name == lock_name).current_status_id
        status_name = LockStatuses.get(LockStatuses.id == statuses).status_name
        return status_name
    except Locks.DoesNotExist:
        return ""


def get_locks_by_status_name(status_name):
    """
    This method returns the name of the locks that fulfill
    the condition of status

    Parameters
    ----------
    status_name : string
        The name of the status to check, currently either Open or Closed

    Returns
    -------
    lock_names : list of string
        A Python list which contains the name of all the locks that
        satisfies the status condition. If the status name is invalid
        returns None
    """
    try:
        statuses = LockStatuses.get(LockStatuses.status_name == status_name)
        locks = statuses.lock_statuses
        lock_names = []
        for lock in locks:
            lock_names.append(lock.lock_name)
        return lock_names
    except LockStatuses.DoesNotExist:
        return None


def set_lock_status_by_name(lock_name, new_status_name):
    """
    This method updates the status of a lock in the database
    given the lock name.

    Parameters
    ----------
    lock_name : string
        The name of the lock

    new_status_name : string
        The name of the status to update the lock to.
        Currently the options are either Open or Closed

    Returns
    -------
    success : bool
        Returns 1 if the update is successful, -1 otherwise.
        There are couple of reasons which might cause -1, these
        are:
            - Status name not found
            - Lock name not found
            - Database connection failed
    """
    try:
        q = Locks.select().where(Locks.lock_name == lock_name)
        if not q.exists():
            return False
        status = LockStatuses.get(LockStatuses.status_name == new_status_name)
        query = Locks.update(current_status_id=status).where(Locks.lock_name
                                                             == lock_name)
        query.execute()
        old_status = "Closed" if new_status_name == "Open" else "Open"
        # TODO user mock for now, will be fixed, also old status is mock for
        #  now, will be changed later
        add_lock_status_history(lock_name, old_status, new_status_name,
                                "Admin")
        if variables.IS_LOCK_CONNECTED:
            rpc_demo_client.send_command_to_hardware(
                2, True if new_status_name == "Open" else False)
        return True
    except LockStatuses.DoesNotExist:
        return False


def set_status_for_all_locks(new_status_name):
    """
    This method updates the status of all locks in the database

    Parameters
    ----------
    new_status_name : string
        The name of the status to update all locks to.
        Currently the options are either Open or Closed

    Returns
    -------
    success : bool
        Returns True if the update is successful, False otherwise.
        There are couple of reasons which might cause -1, these
        are:
            - Status name not found
            - Database connection failed
    """
    try:
        status = LockStatuses.get(LockStatuses.status_name == new_status_name)
        query = Locks.update(current_status_id=status)
        old_status = "Closed" if new_status_name == "Open" else "Open"
        locks = Locks.select(Locks.lock_name)
        for lock in locks:
            add_lock_status_history(lock.lock_name, old_status,
                                    new_status_name, "Admin")
        query.execute()
        return True
    except LockStatuses.DoesNotExist:
        print("Nooo")
        return False
