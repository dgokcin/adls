from database_files.TableCreator import *
from datetime import datetime


def add_schedule_item_tuple(lock_name, start_date, end_date):
    """
    This method adds a single schedule from a startdate to enddate, in which
    the door remains unlocked.

    Example:
    add_schedule_item_tuple("225", start_date_time, end_date_time)

    Parameters
    ----------
    lock_name : string
        The name of the lock for the schedule object.

    start_date : datetime
        The datetime in which the schedule will start.

    end_date : datetime
        The datetime which schedule will end and the door will be locked again.

    Returns
    -------
    success : bool
        Returns True if the schedule is applicable.
        A schedule is applicable when:
            -It does not conflict with other schedules
            -The lock exists
    """
    try:
        lock = Locks.get(Locks.lock_name == lock_name)
        current_schedules = get_schedules_combined(lock.id, isiso=False)
        addable = validate_schedule(lock_name, start_date, end_date)
        if addable:
            Schedules.create(lock_id=lock, schedule_timestamp=start_date,
                             lock_status_id=1,
                             schedule_status_id=1)
            Schedules.create(lock_id=lock, schedule_timestamp=end_date,
                             lock_status_id=2,
                             schedule_status_id=1)
            return True
        else:
            return False
    except Locks.DoesNotExist as e:
        print(e)
        return False


def validate_schedule(lock_name, start_date, end_date):
    """
    This method validates a schedule before adding it.

    Parameters
    ----------
    lock_name : string
        The name of the lock for the schedule object.

    start_date : datetime
        The datetime in which the schedule will start.

    end_date : datetime
        The datetime which schedule will end and the door will be locked again.

    Returns
    -------
    success : bool
        Returns True if the schedule is applicable.
        A schedule is applicable when:
            -The end_date is ahead of the start_date
        Returns False if not.
    """
    lock = Locks.get(Locks.lock_name == lock_name)
    current_schedules = get_schedules_combined(lock.id, isiso=False,
                                               active=True)
    for schedule in current_schedules:
        start = schedule["start_date"]
        end = schedule["end_date"]
        if not (end_date < start or start_date > end):
            return False
    return True


def add_schedule_item(lock_name, schedule_timestamp, lock_status_name):
    """
    This method adds a single schedule object to the database.
    Please note that according to our implementation, the common
    way to do it is not to give all variables one-by-one; but
    calling the dictionary object with **
    Example:

    status = {"lock_name": "225", "schedule_timestamp": dt,
    "lock_status_name": "Open"}

    add_schedule_item(**status)

    Parameters
    ----------
    lock_name : string
        The name of the lock for the schedule object.

    schedule_timestamp : datetime
        The datetime in which the schedule should be called.

    lock_status_name : string
        The new status to set the lock to, currently it is
        either Open or Closed.

    Returns
    -------
    success : bool
        Returns True if the insert is successful, False otherwise.
        There are couple of reasons which might cause -1, these
        are:
            - Status name not found
            - Database connection failed
            - Lock Name not found
    """
    try:
        lock = Locks.get(Locks.lock_name == lock_name)
        status = LockStatuses.get(LockStatuses.status_name == lock_status_name)
        schedule_status = ScheduleStatuses.get(ScheduleStatuses.
                                               schedule_status_name ==
                                               "Active")
        Schedules.create(lock_id=lock, schedule_timestamp=schedule_timestamp,
                         lock_status_id=status,
                         schedule_status_id=schedule_status)
        return True
    except Locks.DoesNotExist:
        return False


def add_new_schedule_items(schedule_items):
    """
    This method adds all the schedule objects in the list to the database.
    Please note that in the for loop, we first call the item since calling
    it customly returns the class as a dict object instead of ScheduleAction

    Parameters
    ----------
    schedule_items : list
        A list of Dictionary Objects, usually
        passed from schedule_parser.

    Returns
    -------
    success : bool
        True if all the updates are successful, False if even 1 fails.
        The system also rollbacks all updates if one fails so all of them
        must be valid in order to be able to execute the update.
    """
    with db.atomic() as transaction:
        # status = ScheduleStatuses.get(ScheduleStatuses.
        #                               schedule_status_name == "Inactive")
        # query = Schedules.update(schedule_status_id=status)
        # query.execute()
        for kl in range(0, len(schedule_items), 2):
            star = schedule_items[kl]
            endo = schedule_items[kl + 1]
            success = add_schedule_item_tuple(star["lock_name"],
                                              star["schedule_timestamp"],
                                              endo["schedule_timestamp"])
            if not success:
                transaction.rollback()
                return False
        return True


def get_lock_schedule_at_time(time):
    """
    This method gets the available schedules for all the locks at a given time.

    Parameters
    ----------
    time : datetime

    Returns
    -------
    d : dict
        The keys are the unique lock names and the values are lock_status_ids.
    """
    d = {}
    time_parsed = datetime(time.year, time.month, time.day, time.hour,
                           time.minute, 0, 0)

    sch = Schedules.select(Schedules.lock_id,
                           Schedules.lock_status_id).where(
        Schedules.schedule_timestamp == time_parsed,
        Schedules.schedule_status_id == 1)

    for s in sch:
        d[s.lock_id.lock_name] = s.lock_status_id.id

    return d


def get_current_lock_schedule():
    """
    This method gets the available schedules for all the locks at current
    time.

    Returns
    -------
    d : dict
        The keys are the unique lock names and the values are lock_status_ids.
    """
    now = datetime.now()

    d = {}
    current_date_parsed = datetime(now.year, now.month, now.day, now.hour,
                                   now.minute, 0, 0)

    sch = Schedules.select(Schedules.lock_id,
                           Schedules.lock_status_id,
                           Schedules.id).where(
        Schedules.schedule_timestamp == current_date_parsed,
        Schedules.schedule_status_id == 1)

    for s in sch:
        d[s.lock_id.lock_name] = (s.lock_status_id.id, s.id)

    return d


def get_lock_schedule_history():
    """
    This method gets all the information about the schedules in the database

    Returns
    -------
    d : dict
        The returned dictionary is a dictionary of dictionaries, which hold
        information about each schedule item in the database.
    """
    lst = []
    d = {}
    sch = Schedules.select(Schedules).where(Schedules.schedule_status_id == 2)

    for s in sch:
        # print(s.lock_status_id.status_name)
        tmp = dict()
        tmp['id'] = s.id
        tmp['name'] = s.lock_id.lock_name
        tmp['location'] = s.lock_id.lock_location
        tmp['timestamp'] = s.schedule_timestamp
        tmp['lock_status'] = s.lock_status_id.status_name
        tmp['schedule_status'] = s.schedule_status_id.schedule_status_name

        lst.append(tmp)

    d['data'] = lst

    return d


def update_schedule_item(schedule_id, lock_name, start_datetime, end_datetime):
    """
    This method updates a schedule's lock_name, start_date and end_date by the
    schedule_id. We utilize this method when we want to change a schedule.
    For instance a class may move between times and classes.

    Parameters
    ----------
    schedule_id : string
    lock_name : string
    start_datetime : datetime
    end_datetime : datetime

    Returns
    -------
    success : bool

    Returns True if the schedule exists and the update has been successful.
    False if not.
    """
    try:
        schedule_id = int(schedule_id)
        idx = Locks.get(Locks.lock_name == lock_name)
        Schedules.update(schedule_timestamp=start_datetime,
                         schedule_status_id=1, lock_id=idx).where(
            Schedules.id == schedule_id).execute()
        Schedules.update(schedule_timestamp=end_datetime,
                         schedule_status_id=1, lock_id=idx).where(
            Schedules.id == schedule_id + 1).execute()
        return True
    except Schedules.DoesNotExist:
        return False


def trigger_schedule_status(schedule_id):
    """
    This method sends a manual trigger on the schedule status,
    turns to lock or unlock depending on it's state.

    Parameters
    ----------
    schedule_id : string

    Returns
    -------
    success : bool

    Returns True if the schedule exists and the status has been
    updated.
    False if not.
    """
    try:
        trigger = {1: 2, 2: 1}
        idx = Schedules.get(Schedules.id == schedule_id).schedule_status_id.id
        new_idx = trigger[idx]
        Schedules.update(schedule_status_id=new_idx).where(
            Schedules.id == schedule_id).execute()
        Schedules.update(schedule_status_id=new_idx).where(
            Schedules.id == schedule_id + 1).execute()
        return True
    except Schedules.DoesNotExist:
        return False


def get_selected_lock_schedule(schedule_id):
    """
    This method returns the lock schedule according to
    user's selection by the id of the schedule.

    Parameters
    ----------
    schedule_id : string

    Returns
    -------
    success : dict

    Returns a dictionary of the following information
    that belongs to the schedule:
        -door_name: the name of the door that the schedule
        is assigned.
        -start_date: the date the schedule starts
        -end_date: the end of the schedule.
    Returns none if not found.
    """
    try:
        schedule_start = Schedules.get(Schedules.id == schedule_id)
        schedule_end = Schedules.get(Schedules.id == schedule_id + 1)
        return {"door_name": schedule_start.lock_id.lock_name,
                "start_date": schedule_start.schedule_timestamp,
                "end_date": schedule_end.schedule_timestamp}
    except Schedules.DoesNotExist:
        return None


def get_schedules_combined(lock_id="", isiso=True, active=True):
    """
    This method either returns a single schedule
    from the lock_id or returns all the schedules.

    Parameters
    ----------
    schedule_id : string

    Returns
    -------
    success : list

    Returns a list of dictionaries of the following
    information that belongs to the schedules selected:
        -door_name: the name of the door that the schedule
        is assigned.
        -start_date: the date the schedule starts
        -end_date: the end of the schedule.
        -id: id of the schedule.
    Returns none if not found.
    """
    a_s = {True: 1, False: 2}[active]
    if lock_id != "":
        schedules = Schedules.select().where(Schedules.lock_id ==
                                             lock_id,
                                             Schedules.schedule_status_id
                                             == a_s).execute()
    else:
        schedules = Schedules.select().where(Schedules.schedule_status_id
                                             == a_s).execute()
    data = []
    for i in range(0, len(schedules), 2):
        sch = schedules[i]
        end_schedule = schedules[i + 1]
        temp = {
            "door_name": sch.lock_id.lock_name,
            "start_date": sch.schedule_timestamp.isoformat() if isiso else
            sch.schedule_timestamp,
            "end_date": end_schedule.schedule_timestamp.isoformat() if isiso
            else end_schedule.schedule_timestamp,
            "id": sch.id}
        data.append(temp)
    return data
