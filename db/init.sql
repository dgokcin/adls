CREATE DATABASE IF NOT EXISTS ADLS;
USE ADLS;

-- usertypes: table
CREATE TABLE IF NOT EXISTS `usertypes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_type_name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- users: table
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(500) COLLATE utf8_bin NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `surname` varchar(255) COLLATE utf8_bin NOT NULL,
  `company_id` varchar(255) COLLATE utf8_bin NOT NULL,
  `address` text COLLATE utf8_bin NOT NULL,
  `user_type_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  KEY `users_user_type_id` (`user_type_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_type_id`) REFERENCES `usertypes` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- lockstatuses: table
CREATE TABLE IF NOT EXISTS `lockstatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status_name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- locks: table
CREATE TABLE IF NOT EXISTS `locks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lock_name` varchar(255) COLLATE utf8_bin NOT NULL,
  `lock_location` varchar(255) COLLATE utf8_bin NOT NULL,
  `current_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `locks_current_status_id` (`current_status_id`),
  CONSTRAINT `locks_ibfk_1` FOREIGN KEY (`current_status_id`) REFERENCES `lockstatuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- No native definition for element: locks_current_status_id (index)


-- lockstatushistory: table
CREATE TABLE IF NOT EXISTS `lockstatushistory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lock_id` int(11) NOT NULL,
  `action_timestamp` datetime NOT NULL,
  `prev_status_id` int(11) NOT NULL,
  `new_status_id` int(11) NOT NULL,
  `action_performed_by_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `lockstatushistory_lock_id` (`lock_id`),
  KEY `lockstatushistory_prev_status_id` (`prev_status_id`),
  KEY `lockstatushistory_new_status_id` (`new_status_id`),
  KEY `lockstatushistory_action_performed_by_id` (`action_performed_by_id`),
  CONSTRAINT `lockstatushistory_ibfk_1` FOREIGN KEY (`lock_id`) REFERENCES `locks` (`id`),
  CONSTRAINT `lockstatushistory_ibfk_2` FOREIGN KEY (`prev_status_id`) REFERENCES `lockstatuses` (`id`),
  CONSTRAINT `lockstatushistory_ibfk_3` FOREIGN KEY (`new_status_id`) REFERENCES `lockstatuses` (`id`),
  CONSTRAINT `lockstatushistory_ibfk_4` FOREIGN KEY (`action_performed_by_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- schedulestatuses: table
CREATE TABLE IF NOT EXISTS `schedulestatuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schedule_status_name` varchar(255) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

-- schedules: table
CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `lock_id` int(11) NOT NULL,
  `schedule_timestamp` datetime NOT NULL,
  `lock_status_id` int(11) NOT NULL,
  `schedule_status_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `schedules_lock_id` (`lock_id`),
  KEY `schedules_lock_status_id` (`lock_status_id`),
  KEY `schedules_schedule_status_id` (`schedule_status_id`),
  CONSTRAINT `schedules_ibfk_1` FOREIGN KEY (`lock_id`) REFERENCES `locks` (`id`),
  CONSTRAINT `schedules_ibfk_2` FOREIGN KEY (`lock_status_id`) REFERENCES `lockstatuses` (`id`),
  CONSTRAINT `schedules_ibfk_3` FOREIGN KEY (`schedule_status_id`) REFERENCES `schedulestatuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=361 DEFAULT CHARSET=utf8 COLLATE=utf8_bin
;

INSERT INTO ADLS.usertypes (id, user_type_name) VALUES (1, 'Admin');
INSERT INTO ADLS.usertypes (id, user_type_name) VALUES (2, 'Normal');
INSERT INTO ADLS.users (id, email, password, name, surname, company_id, address, user_type_id) VALUES (1, 'dummy_admin@dummy.com', '30dbc5bee24e28de2ee2864c10a0facae4a7378a4edb87bcdf46494778c538ccc8fe16896d54e6ee60faf3583a8b90311b2dc8b727d79c389ed4aa82e0ab207f8d51c9e3a53b314e99263da2a19d4d7a0668eaed9a3d7bf4c09820bda3694a58', 'Admin', 'Admin', '1', 'Selam', 1);
INSERT INTO ADLS.users (id, email, password, name, surname, company_id, address, user_type_id) VALUES (2, 'dummy_regular@dummy.com', '12345678', 'Regular', 'Regular', '1', 'Selam', 2);
INSERT INTO ADLS.lockstatuses (id, status_name) VALUES (1, 'Open');
INSERT INTO ADLS.lockstatuses (id, status_name) VALUES (2, 'Closed');
INSERT INTO ADLS.locks (id, lock_name, lock_location, current_status_id) VALUES (5, '225', 'AB1', 2);
INSERT INTO ADLS.locks (id, lock_name, lock_location, current_status_id) VALUES (6, '230', 'AB2', 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (2, 5, '2020-04-15 18:12:50', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (3, 5, '2020-04-15 22:08:55', 2, 1, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (4, 6, '2020-04-15 22:08:56', 2, 1, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (5, 5, '2020-04-15 22:09:39', 2, 1, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (6, 6, '2020-04-15 22:09:40', 2, 1, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (11, 5, '2020-04-15 22:10:51', 2, 1, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (12, 5, '2020-04-15 22:11:03', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (13, 5, '2020-04-15 22:11:28', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (14, 5, '2020-04-15 22:11:56', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (15, 5, '2020-04-15 22:12:03', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (20, 5, '2020-04-15 22:13:25', 1, 2, 1);
INSERT INTO ADLS.lockstatushistory (id, lock_id, action_timestamp, prev_status_id, new_status_id, action_performed_by_id) VALUES (25, 5, '2020-04-15 22:17:10', 1, 2, 1);
INSERT INTO ADLS.schedulestatuses (id, schedule_status_name) VALUES (1, 'Active');
INSERT INTO ADLS.schedulestatuses (id, schedule_status_name) VALUES (2, 'Inactive');
INSERT INTO ADLS.schedules (id, lock_id, schedule_timestamp, lock_status_id, schedule_status_id) VALUES (45, 5, '2020-03-28 18:00:00', 1, 1);


