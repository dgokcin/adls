function getMsgHTML(name, msg, side) {
    var msg_temple = `
        <div class="card">
             <div class="card-body">
                 <h6 class="card-subtitle mb-2 text-muted text-${side}">${name}</h6>
                 <p class="card-text float-${side}" style="font-size: 60px;">${msg}</p>
             </div>
        </div>
        `;
    return msg_temple;
}
function sendMessage(name, msg) {
    $('#messagesList').append(getMsgHTML(name, msg, 'right'));
}

function receiveMessage(name, msg) {
    $('#messagesList').append(getMsgHTML(name, msg, 'left'));
}